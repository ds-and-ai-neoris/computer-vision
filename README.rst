:Authors: NEORIS DS & AI
:Web site: https://bitbucket.org/ds-and-ai-neoris/computer-vision/src/master/

Computer vision
===================

This repos serves as a resource for data scientists, analysts and others interested in learning more about computer
vision.  The two main goals of this resource are:

1. To ensure prerequisite knowledge for applied computer vision lives in one place and has a logical flow
2. To collate free training resources in a organized and useful way
3. To provide examples and exercises that will accelerate your journey into the world of computer vision

Software, packages and more
-------------------------------

Development environment
^^^^^^^^^^^^^^^^^^^^^^^^

1. `Install Git <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`_

2. Install Python

    - `Python on Windows <https://docs.python-guide.org/starting/install3/win/#install3-windows>`_
    - `Python on Linux <https://docs.python.org/3/using/unix.html#on-linux>`_
    - `Python on OSX <https://docs.python-guide.org/starting/install3/osx/>`_

3. Install a `text editor <https://en.wikipedia.org/wiki/Text_editor>`_.  Any of the following will work:

    - `Atom <https://atom.io/>`_
    - `PyCharm <https://www.jetbrains.com/help/pycharm/installation-guide.html>`_
    - `VSCode <https://code.visualstudio.com/download>`_

4. Optionally you can also `Install Docker <https://docs.docker.com/get-docker/>`_

5. Finally ensure that the packages you will need are installed.

.. code-block:: bash

    ~$ pip install -r requirements.txt
    ~$ python setup.py install


Additional resources
----------------------

    - `More about Python on Windows <https://docs.python.org/3/using/windows.html>`_


